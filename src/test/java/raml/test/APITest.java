package raml.test;

import io.restassured.builder.RequestSpecBuilder;
import org.junit.Test;

import java.util.function.Function;

public class APITest {

    private final String API_KEY = "61151f4e-6a4d-42d2-b43d-0945a5e7404b";
    private final String BASE_URI = "https://api.rasp.yandex.net/v3.0";

    @Test
    public void testSearchReturns200() {
        api().search()
            .withApikey(API_KEY)
            .withFrom("c65")
            .withTo("c66")
            .withLimit("2")
            .get(Function.identity())
            .prettyPeek()
            .then().statusCode(200);
    }

    @Test
    public void testScheduleReturns200WithOnlyRequiredParameters() {
        api().schedule()
            .withApikey(API_KEY)
            .withStation("s9610384")
            .get(Function.identity())
            .prettyPeek()
            .then().statusCode(200);
    }

    @Test
    public void testThreadReturns404WithWrongUID() {
        api().thread()
            .withApikey(API_KEY)
            .withUid("")
            .get(Function.identity())
            .prettyPeek()
            .then().statusCode(404);
    }

    private ApiYandexRasp api() {
        return ApiYandexRasp.yandexrasp(
                ApiYandexRasp.Config.yandexraspConfig().withReqSpecSupplier(() ->
                        new RequestSpecBuilder().setBaseUri(BASE_URI)
                )
        );
    }
}
